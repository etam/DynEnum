#ifndef ETAM_DYN_ENUM_HPP
#define ETAM_DYN_ENUM_HPP

#include <initializer_list>
#include <stdexcept>
#include <string>

#include <boost/container/flat_set.hpp>
#include <boost/iterator/iterator_facade.hpp>


namespace etam {

template <typename Derived>
class DynEnum
{
private:
    boost::container::flat_set<std::string> mSet;

public:
    class Value
        : public boost::iterator_facade<Value, const Value&, boost::random_access_traversal_tag>
    {
    private:
        using Base = boost::iterator_facade<Value, const Value&, boost::random_access_traversal_tag>;

    public:
        using value_type = typename Base::value_type;
        using reference = typename Base::reference;
        using pointer = typename Base::pointer;
        using difference_type = typename Base::difference_type;
        using iterator_category = typename Base::iterator_category;

    private:
        friend DynEnum;
        using It = typename decltype(DynEnum::mSet)::const_iterator;

        It mIt;

        Value(It it)
            : mIt{it}
        {}

        friend boost::iterator_core_access;

        const Value& dereference() const
        {
            return *this;
        }

        bool equal(const Value& other) const
        {
            return mIt == other.mIt;
        }

        void increment()
        {
            ++mIt;
        }

        void decrement()
        {
            --mIt;
        }

        void advance(difference_type n)
        {
            mIt += n;
        }

        difference_type distance_to(const Value& other) const
        {
            return other.mIt - mIt;
        }

    public:
        Value() = default;

        const std::string& name() const
        {
            return *mIt;
        }
    };

    DynEnum(std::initializer_list<std::string> l)
        : mSet{l}
    {}

    DynEnum() = delete;
    DynEnum(const DynEnum&) = delete;
    DynEnum(DynEnum&&) = default;
    DynEnum& operator=(const DynEnum&) = delete;
    DynEnum& operator=(DynEnum&&) = default;
    ~DynEnum() = default;

    Value get(const std::string& name) const
    {
        const auto it = mSet.find(name);
        if (it == mSet.end()) {
            throw std::out_of_range('"' + name + "\" not found");
        }
        return {it};
    }

    Value begin() const
    {
        return {mSet.cbegin()};
    }

    Value end() const
    {
        return {mSet.cend()};
    }
};

} // namespace etam

#define ETAM_DECLARE_DYNENUM(singular_name) \
class singular_name##s : public etam::DynEnum<singular_name##s> { \
    using Base = etam::DynEnum<singular_name##s>; \
    using Base::Base; \
}; \
using singular_name = singular_name##s::Value;

#endif // ETAM_DYN_ENUM_HPP
