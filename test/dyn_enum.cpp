#include <boost/test/unit_test.hpp>

#include "dyn_enum.hpp"

#include <ostream>
#include <stdexcept>
#include <string>
#include <vector>


namespace {

ETAM_DECLARE_DYNENUM(Enum)

struct Fixture
{
    const Enums e;

    Fixture()
        : e{"Ala", "ma", "kota"}
    {}
};

} // namespace

namespace etam {
inline namespace {

std::ostream& operator<<(std::ostream& ostream, ::Enum value)
{
    return ostream << value.name();
}

} // inline namespace
} // namespace etam



BOOST_FIXTURE_TEST_SUITE( DynEnum, Fixture )

BOOST_AUTO_TEST_CASE( get_name )
{
    for (const char* str : {"Ala", "ma", "kota"}) {
        BOOST_CHECK_EQUAL(e.get(str).name(), std::string{str});
    }
}

BOOST_AUTO_TEST_CASE( get_invalid )
{
    BOOST_CHECK_THROW(e.get("bla"), std::out_of_range);
}

BOOST_AUTO_TEST_CASE( lesser )
{
    BOOST_CHECK_LE(e.get("Ala"), e.get("kota"));
    BOOST_CHECK_LE(e.get("Ala"), e.get("ma"));
    BOOST_CHECK_LE(e.get("Ala"), e.end());
    BOOST_CHECK_LE(e.get("kota"), e.get("ma"));
    BOOST_CHECK_LE(e.get("kota"), e.end());
    BOOST_CHECK_LE(e.get("ma"), e.end());
}

BOOST_AUTO_TEST_CASE( increment )
{
    BOOST_CHECK_EQUAL(++e.get("Ala"), e.get("kota"));
    BOOST_CHECK_EQUAL(++e.get("kota"), e.get("ma"));
    BOOST_CHECK_EQUAL(++e.get("ma"), e.end());
}

BOOST_AUTO_TEST_CASE( iterators )
{
    const auto v = std::vector<Enum>{e.get("Ala"), e.get("kota"), e.get("ma")};
    BOOST_CHECK_EQUAL_COLLECTIONS(e.begin(), e.end(), v.begin(), v.end());
}

BOOST_AUTO_TEST_SUITE_END() // DynEnum
